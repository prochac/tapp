import React from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AirCalc from "./container/AirCalc";
import AirCalc2 from "./container/AirCalc2";
import Paper from "@material-ui/core/Paper";
import SwipeableViews from 'react-swipeable-views';
// import Container from "@material-ui/core/Container";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

function App() {
    const classes = useStyles();
    const theme = useTheme();
    const [value, setValue] = React.useState(0);

    return (
        <>
            <CssBaseline/>
            <div className={classes.root}>
                <Paper square>
                    <Tabs
                        variant="fullWidth"
                        value={value}
                        indicatorColor="primary"
                        textColor="primary"
                        onChange={(e, v) => {
                            setValue(v);
                        }}
                        aria-label="disabled tabs example"
                    >
                        <Tab label="Obdélník"/>
                        <Tab label="Kruh"/>
                    </Tabs>
                </Paper>
                <SwipeableViews
                    axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                    index={value}
                    onChangeIndex={(i) => setValue(i)}
                    style={{height: '100vh'}}
                >
                    <div
                        hidden={value !== 0}
                    >
                        <AirCalc/>
                    </div>
                    <div
                        hidden={value !== 1}
                    >
                        <AirCalc2/>
                    </div>
                </SwipeableViews>
            </div>
        </>
    );
}

export default App;
