import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import MaskedInput from 'react-text-mask';
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
}));

const AirCalc = () => {
    const classes = useStyles();

    const [flow, setFlow] = useState(2400);
    const [velocity, setVelocity] = useState(4.0);
    const [sizeA, setSizeA] = useState(450);
    const [sizeB, setSizeB] = useState(400);
    const [calcVelo, setCalcVelo] = useState(null);

    useEffect(() => {
        setCalcVelo(((flow / 3600) / (sizeA / 1000 * sizeB / 1000))
            .toFixed(2));
    }, [flow, velocity, sizeA, sizeB])

    useEffect(() => {
        if (+sizeB > +sizeA) {
            setSizeA(sizeB);
            setSizeB(sizeA);
        }
    }, [sizeA, sizeB])

    return (
        <form className={classes.root} noValidate autoComplete="off">
            <TextField
                label="Průtok"
                type="number"
                value={flow}
                onChange={(e) =>
                    setFlow(e.target.value)
                }
                InputProps={{
                    endAdornment: <InputAdornment position="end">m^3</InputAdornment>
                }}
            />
            <br/>
            <TextField
                label="Rychlost"
                type="number"
                value={velocity}
                onChange={(e) =>
                    setVelocity(e.target.value)
                }
                InputProps={{
                    endAdornment: <InputAdornment position="end">m/s</InputAdornment>
                }}
            /><br/>
            <TextField
                label="Preferovaný rozměr"
                type="text"
                pattern="\d*"
                value={sizeA + 'x' + sizeB}
                onChange={(e) => {
                    const [a, b] = e.target.value.split("x");
                    setSizeA(a);
                    setSizeB(b);
                }}
                InputProps={{
                    endAdornment: <InputAdornment position="end">mm</InputAdornment>,
                    inputComponent: TextMaskCustom,
                }}
            /><br/>
            <TextField
                label="Aktuální rychlost"
                type="number"
                value={calcVelo}
                InputProps={{
                    endAdornment: <InputAdornment position="end">m/s</InputAdornment>,
                    readOnly: true,
                }}
            /><br/>
        </form>
    );
};

function TextMaskCustom(props) {
    const {inputRef, ...other} = props;

    return (
        <MaskedInput
            {...other}
            ref={(ref) => {
                inputRef(ref ? ref.inputElement : null);
            }}
            mask={[/[1-9]/, /\d/, /\d/, 'x', /[1-9]/, /\d/, /\d/,]}
            placeholderChar={'\u2000'}
            showMask
        />
    );
}

TextMaskCustom.propTypes = {
    inputRef: PropTypes.func.isRequired,
};

export default AirCalc;