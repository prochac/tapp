import React, {useState, useEffect} from 'react';
import {lowerRangeFor, higherRangeFor} from "../dimensionalRange";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
}));

const AirCalc = () => {
    const classes = useStyles();

    const [flow, setFlow] = useState(2400);
    const [velocity, setVelocity] = useState(4.0);

    const [lowDiameter, setLowDiameter] = useState("");
    const [lowDiameterVelo, setLowDiameterVelo] = useState(null);
    const [highDiameter, setHighDiameter] = useState("");
    const [highDiameterVelo, setHighDiameterVelo] = useState(null);

    useEffect(() => {
        const s = (flow * 1000) / (velocity * 3.6);
        const d = Math.sqrt(4 * s / Math.PI);
        setLowDiameter(lowerRangeFor(d));
        setHighDiameter(higherRangeFor(d));
    }, [flow, velocity])

    useEffect(() => {
        const v = (flow * 1000) / (0.9 * Math.PI * Math.pow(lowDiameter, 2));
        setLowDiameterVelo(v.toFixed(2))
    }, [flow, lowDiameter]);

    useEffect(() => {
        const v = (flow * 1000) / (0.9 * Math.PI * Math.pow(highDiameter, 2));
        setHighDiameterVelo(v.toFixed(2))
    }, [flow, highDiameter]);

    return (
        <form className={classes.root} noValidate autoComplete="off">
            <TextField
                label="Průtok"
                type="number"
                value={flow}
                onChange={(e) =>
                    setFlow(e.target.value)
                }
                InputProps={{
                    endAdornment: <InputAdornment position="end">m^3</InputAdornment>
                }}
            />
            <br/>

            <TextField
                label="Rychlost"
                type="number"
                value={velocity}
                onChange={(e) =>
                    setVelocity(e.target.value)
                }
                InputProps={{
                    endAdornment: <InputAdornment position="end">m/s</InputAdornment>
                }}
            /><br/>

            <label htmlFor="sizeA">Nabídka Preferovaný rozměr</label>
            <br/>

            <TextField
                // label="lowDiameter"
                type="number"
                value={lowDiameter}
                onChange={(e) =>
                    setLowDiameter(e.target.value)
                }
                InputProps={{
                    startAdornment: <InputAdornment position="start">Ø</InputAdornment>,
                    endAdornment: <InputAdornment position="end">mm</InputAdornment>,
                    shrink: true,
                }}
            /><br/>

            <TextField
                label="Rychlost"
                type="number"
                value={lowDiameterVelo}
                InputProps={{
                    endAdornment: <InputAdornment position="end">m/s</InputAdornment>,
                    readOnly: true,
                }}
            /><br/>

            <TextField
                // label="highDiameter"
                type="number"
                value={highDiameter}
                onChange={(e) =>
                    setHighDiameter(e.target.value)
                }
                InputProps={{
                    startAdornment: <InputAdornment position="start">Ø</InputAdornment>,
                    endAdornment: <InputAdornment position="end">mm</InputAdornment>,
                    shrink: true,
                }}
            /><br/>
            <TextField
                label="Rychlost"
                type="number"
                value={highDiameterVelo}
                InputProps={{
                    endAdornment: <InputAdornment position="end">m/s</InputAdornment>,
                    readOnly: true,
                }}
            /><br/>
        </form>
    );
};

export default AirCalc;